# frozen_string_literal: true

module PlentyClient
  module Tag
    include PlentyClient::Endpoint
    include PlentyClient::Request

    LIST_TAGS  = '/tags'
    CREATE_TAG = '/tags'
    UPDATE_TAG  = '/tag/{tagId}'
    DELETE_TAG  = '/tags/{tagId}'

    class << self
      def list(headers = {}, &block)
        get(build_endpoint(LIST_TAGS), headers, &block)
      end

      def create(body = {})
        post(build_endpoint(CREATE_TAG), body)
      end

      def update(tag_id, body = {})
        put(build_endpoint(UPDATE_TAG, tag: tag_id), body)
      end

      def delete(tag_id, body = {})
        delete(build_endpoint(DELETE_TAG, tag: tag_id))
      end
    end
  end
end
